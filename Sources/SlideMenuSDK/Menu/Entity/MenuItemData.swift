//
//  MenuItemData.swift
//  
//
//  Created by Luiz Fernando dos Santos on 03/02/20.
//

import UIKit

public struct MenuItemData {
    let id: Int
    let title: String
    let controller: BaseMenuViewController
    let tableViewCell: UITableViewCell?
    
    public init(id: Int,
                title: String,
                controller: BaseMenuViewController,
                tableViewCell: UITableViewCell? = nil) {
        self.id = id
        self.title = title
        self.controller = controller
        self.tableViewCell = tableViewCell
    }
}
