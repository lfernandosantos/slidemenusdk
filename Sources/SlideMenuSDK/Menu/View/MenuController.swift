//
//  MenuController.swift
//  
//
//  Created by Luiz Fernando dos Santos on 03/02/20.
//

import UIKit

final class MenuController: UIViewController {
    
    var tableView: UITableView!
    var delegate: MenuDelegate?
    var items: [MenuItemData]
    var header: UIView?
    var footer: UIView?
    var bakgroundColor: UIColor?
    
    override func viewDidLoad() {
        configureTableView()
        self.view.backgroundColor = #colorLiteral(red: 0.1900961697, green: 0.3769851923, blue: 0.3746039271, alpha: 1)
    }
    
    init(menuItemsData: [MenuItemData],
         headerMenu: UIView?,
         footerMenu: UIView?,
         backgroundColor: UIColor? = nil) {
        self.items = menuItemsData
        self.header = headerMenu
        self.footer = footerMenu
        self.bakgroundColor = backgroundColor
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureTableView() {
        tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = self.bakgroundColor
        self.view.addSubview(tableView)
        tableView.reloadData()
        tableView.frame = self.view.frame
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .top)
    }
}

//  MARK: - TableViewDelegate and TableViewDataSource

extension MenuController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if let header = self.header {
            return header.frame.height
        }
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if let header = self.footer {
            return header.frame.height
        }
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return self.footer
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = items[indexPath.row].tableViewCell {
            return cell
        }
        
        let cell = UITableViewCell()
        cell.backgroundColor = .clear
        cell.textLabel?.text = items[indexPath.row].title
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.handleMenuToogle(for: items[indexPath.row])
    }
}
