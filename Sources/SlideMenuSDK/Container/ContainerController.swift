//
//  ContainerController.swift
//  
//
//  Created by Luiz Fernando dos Santos on 03/02/20.
//

import UIKit

public final class ContainerController: UIViewController {
    
    // MARK: - Properties
    
    private var menuController: MenuController!
    private var centerController: UIViewController!
    private var isExpand: Bool = false
    private var controllers: [BaseMenuViewController] = []
    private let headerMenu: UIView?
    private let footerMenu: UIView?
    private var menuItems: [MenuItemData] = []
    private var statusBarStyle: UIStatusBarStyle
    private var shortViewSize: CGFloat = 256
    private let needResize: Bool = false

    private var sizeHeightToReduce: CGFloat { return (self.view.frame.height / 6) }
    private var cornerToReduce: CGFloat { return (8 / self.view.frame.width) }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return self.statusBarStyle
    }
    
    override public var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .slide
    }
    
    override public var prefersStatusBarHidden: Bool {
        return isExpand
    }
    
    public required init(menuItems: [MenuItemData],
                  headerMenu: UIView? = nil,
                  footerMenu: UIView? = nil,
                  statusBarStyle: UIStatusBarStyle = .default) {
        self.menuItems = menuItems
        self.statusBarStyle = statusBarStyle
        self.headerMenu = headerMenu
        self.footerMenu = footerMenu
        super.init(nibName: nil, bundle: nil)
        self.modalPresentationStyle = .fullScreen
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func viewDidLoad() {
        self.view.backgroundColor = .red
        self.configureHomeController()
        self.createPanGestureRecognizer(targetView: self.view)
        self.modalPresentationStyle = .fullScreen
    }
    
    private func configureHomeController() {
        guard let homeViewController = menuItems.first?.controller else { return }
        homeViewController.delegate = self
        centerController = UINavigationController(rootViewController: homeViewController)
        view.addSubview(centerController.view)
        addChild(centerController)
        centerController.didMove(toParent: self)
    }
    
    private func configureMenuController() {
        if menuController == nil {
            menuController = MenuController(menuItemsData: menuItems, headerMenu: headerMenu, footerMenu: footerMenu)
            menuController.delegate = self
            view.insertSubview(menuController.view, at: 0)
            addChild(menuController)
            menuController.didMove(toParent: self)
        }
    }
    
    private func expandMenu(changeHeight: Bool) {
        UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .transitionFlipFromLeft, animations: {

            self.centerController.view.frame.origin.x = self.centerController.view.frame.width - 80
           
            let center = self.centerController.view.center
            if changeHeight {
                self.centerController.view.frame.size.height = self.view.frame.height - (self.view.frame.height / 6)
                self.centerController.view.center = center
            }
            
            self.centerController.view.layer.cornerRadius = 8
        }, completion: nil)
    }
    
    private func closeMenu(changeHeight: Bool, completion: @escaping ()-> Void) {
        UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .transitionFlipFromRight, animations: {
            
            if changeHeight {
                self.centerController.view.frame.size.height = self.view.frame.height
            }
            
            self.centerController.view.frame.origin.x = 0
            let center = self.view.center
            
            self.centerController.view.center = center
            self.centerController.view.layer.cornerRadius = 0
        }) { (_) in
            completion()
        }
    }
    
    private func showMenuController(shouldExpand: Bool, menuItemData: MenuItemData?, changeHeight: Bool = true) {
        if shouldExpand {
            self.expandMenu(changeHeight: changeHeight)
        } else {
            self.closeMenu(changeHeight: changeHeight) {
                guard let item = menuItemData else { return }
                self.didSelectedMenuOption(item)
            }
        }
        self.animatedStatusBar()
    }
    
    private func didSelectedMenuOption(_ menuItemData: MenuItemData) {
        view.subviews.last?.removeFromSuperview()
        navigationController?.viewControllers.last?.removeFromParent()
        let controller = menuItems[menuItemData.id].controller
        centerController = UINavigationController(rootViewController: controller)
        controller.delegate = self
        view.addSubview(centerController.view)
        addChild(centerController)
        centerController.didMove(toParent: self)
        
    }
    
    private func animatedStatusBar() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            self.setNeedsStatusBarAppearanceUpdate()
        }, completion: nil)
    }
    
    private func createPanGestureRecognizer(targetView: UIView) {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(panGesture:)))
        targetView.addGestureRecognizer(panGesture)
    }

    private func transitionViewResize(_ velocity: CGPoint) {
        if(velocity.x < 0) {
            let partOfSizeToReduce = self.sizeHeightToReduce / (self.view.frame.width - 80)
            
            self.centerController.view.bounds.size.height += partOfSizeToReduce
            self.centerController.view.layer.cornerRadius += cornerToReduce
            self.centerController.view.center = self.view.center
        } else {
            let partOfSizeToReduce = self.sizeHeightToReduce / (self.view.frame.width - 80)
            
            self.centerController.view.bounds.size.height -= partOfSizeToReduce
            self.centerController.view.layer.cornerRadius -= cornerToReduce
            self.centerController.view.center = self.view.center
        }
    }
    
    @objc func handlePanGesture(panGesture: UIPanGestureRecognizer) {
        self.configureMenuController()
        
        let velocity: CGPoint = panGesture.velocity(in: self.centerController.view)
        
        let translation = panGesture.translation(in: self.centerController.view)
        if isExpand {
            guard translation.x < 0 else { return }
            
            self.transitionViewResize(velocity)
            
            self.centerController.view.frame.origin.x = (self.centerController.view.frame.width - 80) + translation.x
        } else {
            
            guard translation.x > 0 else { return }
            
            self.transitionViewResize(velocity)
            
            self.centerController.view.frame.origin.x = translation.x
        }

        if panGesture.state == UIGestureRecognizer.State.ended {
            if !isExpand {
                if translation.x > (self.view.bounds.width / 4) {
                    self.isExpand = true
                    self.showMenuController(shouldExpand: true, menuItemData: nil, changeHeight: true)
                    
                    
                } else {
                    self.centerController.view.frame.origin.x = 0
                    let center = self.view.center
                    
                    self.centerController.view.center = center
                    self.centerController.view.layer.cornerRadius = 0
                    
                    self.centerController.view.bounds.size.height = self.view.frame.height
                }
            } else {
                if (translation.x * -1) > (self.view.bounds.width / 4) {
                    self.isExpand = false
                    self.showMenuController(shouldExpand: false, menuItemData: nil, changeHeight: true)
                    
                } else {
                    self.centerController.view.frame.origin.x = self.centerController.view.frame.width - 80
                    
                    self.centerController.view.bounds.size.height = self.view.frame.height - (self.view.frame.height / 6)
                }
            }
        }
    }
}

//MARK: - MenuDelegate
extension ContainerController: MenuDelegate {
    func handleMenuToogle(for item: MenuItemData?) {
        self.isExpand = !isExpand
        if isExpand {
            configureMenuController()
        }
        self.showMenuController(shouldExpand: isExpand, menuItemData: item)
    }
}

