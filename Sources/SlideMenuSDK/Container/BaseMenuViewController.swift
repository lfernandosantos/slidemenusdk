//
//  BaseMenuViewController.swift
//  
//
//  Created by Luiz Fernando dos Santos on 03/02/20.
//

import UIKit

protocol MenuDelegate: AnyObject {
    func handleMenuToogle(for item: MenuItemData?)
}

open class BaseMenuViewController: UIViewController {
    
    weak var delegate: MenuDelegate?
    public var titleViewController: String = ""
    private var navigationBarColor: UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    private var navigationBarStyle: UIBarStyle = .default
    private var navigationBarImage: UIImage? = UIImage(systemName: "line.horizontal.3")
    
    public func setupNavigationBar(navigationColor: UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1),
                            navigationStyle: UIBarStyle = .default,
                            navigationImage: UIImage? = UIImage(systemName: "line.horizontal.3")) {
        self.navigationBarColor = navigationColor
        self.navigationBarStyle = navigationStyle
        self.navigationBarImage = navigationImage
        self.configureNavigationBar()
    }
    
    private func configureNavigationBar() {
        if let navbar = navigationController?.navigationBar {
            navbar.barTintColor = self.navigationBarColor
            navbar.barStyle = self.navigationBarStyle
            navigationItem.title = titleViewController
            if let navigationImage = self.navigationBarImage {
                 navigationItem.leftBarButtonItem = UIBarButtonItem(image: navigationImage.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleMenuTootle))
            } else {
                navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .bookmarks, target: self, action: #selector(handleMenuTootle))
            }
        }
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar()
    }
    
    @objc func handleMenuTootle() {
        delegate?.handleMenuToogle(for: nil)
    }
}
