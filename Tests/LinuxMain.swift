import XCTest

import SlideMenuSDKTests

var tests = [XCTestCaseEntry]()
tests += SlideMenuSDKTests.allTests()
XCTMain(tests)
